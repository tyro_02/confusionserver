'use strict';
const authenticate = require('../config/authenticate');
const cors = require('./cors');

var controllerFavourites = require('../controllers/controllerFavourites');

module.exports = function(app) {

    app.route('/favourites/:dishId')
        .options(cors.corsWithOptions, (req, res) => {res.sendStatus(200)})
        .post(cors.corsWithOptions, authenticate.verifyUser, controllerFavourites.addFavourite)
        .delete(cors.corsWithOptions, authenticate.verifyUser, controllerFavourites.removeFavourite)
    
    app.route('/favourites')
        .options(cors.corsWithOptions, (req, res) => {res.sendStatus(200)})
        .post(cors.cors, authenticate.verifyUser, controllerFavourites.addFavourites)
        .get(cors.cors, authenticate.verifyUser, controllerFavourites.getFavourites)
        .delete(cors.cors, authenticate.verifyUser, controllerFavourites.removeFavourites)

    };