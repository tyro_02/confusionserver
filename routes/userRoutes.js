'use strict';
const authenticate = require('../config/authenticate');
const controllerUser = require('../controllers/controllerUsers');
const cors = require('./cors');

module.exports = function(app) {
    app.route('/signup')
        .post(cors.corsWithOptions, controllerUser.register)

    app.route('/login')
        .post(cors.corsWithOptions, controllerUser.login)

    app.route('/users')
        .get(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, controllerUser.getUsers)

    app.route('/users/facebook/token')
        .get(controllerUser.facebookLogin);
};