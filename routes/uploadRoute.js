'use strict';
const authenticate = require('../config/authenticate');
const cors = require('./cors');

var controllerImageUpload = require('../controllers/controllerUpload');

module.exports = function(app) {
    app.route('/imageUpload')
        .options(cors.corsWithOptions, (req, res) => {res.sendStatus(200)})
        .post(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, controllerImageUpload.uploadImage)
};