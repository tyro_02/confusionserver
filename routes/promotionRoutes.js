'use strict';
const authenticate = require('../config/authenticate');
const cors = require('./cors');

var controllerPromotion = require('../controllers/controllerPromotions');

module.exports = function(app) {
    app.route('/promotions')
        .options(cors.corsWithOptions, (req, res) => {res.sendStatus(200)})
        .post(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, controllerPromotion.addPromotion)
        .get(cors.cors, controllerPromotion.getPromotions)

    app.route('/promotion/:id')
        .options(cors.corsWithOptions, (req, res) => {res.sendStatus(200)})
        .get(cors.cors, controllerPromotion.getPromotion)
        .put(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, controllerPromotion.updatePromotion)
        .delete(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, controllerPromotion.deletePromotion)
};