'use strict';
const authenticate = require('../config/authenticate');
const cors = require('./cors');

var controllerLeader = require('../controllers/controllerLeaders');

module.exports = function(app) {
    app.route('/leaders')
        .options(cors.corsWithOptions, (req, res) => {res.sendStatus(200)})
        .post(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, controllerLeader.addLeader)
        .get(cors.cors, controllerLeader.getLeaders)

    app.route('/leader/:id')
        .options(cors.corsWithOptions, (req, res) => {res.sendStatus(200)})
        .get(cors.cors, controllerLeader.getLeader)
        .put(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, controllerLeader.updateLeader)
        .delete(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, controllerLeader.deleteLeader)
};