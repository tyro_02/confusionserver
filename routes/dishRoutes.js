'use strict';
const authenticate = require('../config/authenticate');
const cors = require('./cors');

const controllerDish = require('../controllers/controllerDishes');

module.exports = function(app) {

    app.route('/dishes')
        .options(cors.corsWithOptions, (req, res) => {res.sendStatus(200)})
        .post(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, controllerDish.addDish)
        .get(cors.cors, controllerDish.getDishes)

    app.route('/dish/:id')
        .options(cors.corsWithOptions, (req, res) => {res.sendStatus(200)})
        .get(cors.cors, controllerDish.getDish)
        .put(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, controllerDish.updateDish)
        .delete(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, controllerDish.deleteDish)

    app.route('/dish/:id/comments')
        .options(cors.corsWithOptions, (req, res) => {res.sendStatus(200)})
        .post(cors.corsWithOptions, authenticate.verifyUser, controllerDish.addComment)  
        .get(cors.cors, controllerDish.getComments)     

    app.route('/dish/:id/comments/:commentId')
        .options(cors.corsWithOptions, (req, res) => {res.sendStatus(200)})
        .get(cors.cors, controllerDish.getComment)
        .put(cors.corsWithOptions, authenticate.verifyUser, controllerDish.updateComment)
        .delete(cors.corsWithOptions, authenticate.verifyUser, controllerDish.deleteComment)     
};