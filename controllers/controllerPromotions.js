'use strict';

var Promotion = require('../models/promotion'); 

exports.addPromotion = function(req, res) {
    var p = new Promotion(); 
    p.name = req.body.name;
    p.image = req.body.image;
    p.label = req.body.label;
    p.price = req.body.price;
    p.description = req.body.description;
    p.featured = req.body.featured;

    p.save(function (err, newPromotion) {
        if (err) {
            console.log(err);
            return res.status(400).send({ success: false, message: 'Something Bad Happened!'}); 
        }
        res.status(201).send({ success : true , message: 'New Promotion Inserted Into Database!' }) 
    }) 
}; 

exports.getPromotions = function(req, res) {
    Promotion.find({}, function (err, promotions) {  
        if (err) {  
			console.log(err.errors);
        	return res.status(404).send({ success: false, message: 'Not found!'}); 
        } else {
           return res.status(200).json(promotions);
        }          
    });
}

exports.updatePromotion = function(req, res) {
    Promotion.findOneAndUpdate({_id: req.params.id}, req.body, {new: true}, function(err, updatedPromotion) {
        if (err){
            res.status(404).json({success : false, message : 'Something bad happened' }); 
        } else {
            res.status(200).json({ success : true, message: 'Promotion updated into database!' }) 
        }
        
    });
  };

exports.getPromotion = function(req, res) {
    Promotion.find({ _id: req.params.id }, function (err, promotion) {  
        if (err) {  
        	return res.status(404).json({ success: false, message: 'Not found!'});    
        } else { 
            res.status(200).json(promotion);  
        } 
    })  
};

exports.deletePromotion = function(req, res) {
    Promotion.remove({ _id: req.params.id }, function (err, Promotion) {  
        if (err) {  
            console.log(err.errors);
        	return res.status(404).json({ success: false, message: 'Not found!'});    
        } else { 
            res.status(200).json({ success: true, message: 'Promotion successfully deleted' }); 
        } 
    })  
};