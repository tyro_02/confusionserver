'use strict';

var User = require('../models/user')
var passport = require('passport');
var authenticate = require('../config/authenticate');

exports.register = function (req, res) {
    User.register(new User({ username: req.body.username }),
        req.body.password, (err, user) => {
            if (err) {
                res.status(500).send({ success: false, message: err });
            }
            else {
                if (req.body.firstname)
                    user.firstname = req.body.firstname;
                if (req.body.lastname)
                    user.lastname = req.body.lastname;
                user.save((err, user) => {
                    if (err) {
                        res.status(500).send({ success: false, message: err });
                        return;
                    }
                    passport.authenticate('local')(req, res, () => {
                        res.status(201).send({ success: true, message: 'Registration Successful!' });
                    });
                });
            }
        });
};

exports.login = function (req, res) {
    passport.authenticate('local')(req, res, () => {
        var token = authenticate.getToken({ _id: req.user._id });
        res.status(200).send({ success: true, token: token, message: 'You are successfully logged in!' });
    });
};

exports.getUsers = function (req, res) {
    User.find({})
        .then((users) => {
            res.status(200).send(users);
        }, (err) => next(err))
        .catch((err) => next(err));
};

exports.facebookLogin = function (req, res) {
    console.log('sjdsdhjshj FB')
    passport.authenticate('facebook-token')(req, res, () => {
        if(req.user) {
            var token = authenticate.getToken({ _id: req.user._id });
            res.status(200).send({ success: true, token: token, message: 'You are successfully logged in!' });
        }
    });
};