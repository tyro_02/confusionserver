const multer = require('multer');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'assets/images');
    },

    filename: (req, file, cb) => {
        cb(null, file.originalname)
    }
});

const imageFileFilter = (req, file, cb) => {
    if(!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
        return cb(new Error('You can upload only image files!'), false);
    }
    cb(null, true);
};

const upload = multer({ storage: storage, fileFilter: imageFileFilter});


exports.uploadImage = function (req, res) { 
    upload.single('imageFile')(req,res,(err)=>{
        if(err){
            res.status(404).send({ success: true, message: 'Image upload error!'});
        }else{
            // console.log(req.file);
            res.status(200).send({ success: true, message: 'Image uploaded successfully!'});
        }
   });
};

