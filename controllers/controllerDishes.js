'use strict';

var Dish = require('../models/dish');

exports.addDish = function (req, res) {
    var d = new Dish();
    d.name = req.body.name;
    d.description = req.body.description;
    d.image = req.body.image;
    d.category = req.body.category;
    d.label = req.body.label;
    d.price = req.body.price;
    d.featured = req.body.featured;
    d.comments = req.body.comments;

    d.save(function (err, newDish) {
        if (err) {
            console.log(err);
            return res.status(400).send({ success: false, message: 'Something Bad Happened!' });
        }
        res.status(201).send({ success: true, dish: newDish })
    })
};

exports.getDishes = function (req, res, next) {
    Dish.find({})
        .populate('comments.author')
        .then((dishes) => {
            res.status(200).send(dishes);
        }, (err) => next(err))
        .catch((err) => res.status(400).send('Bad request ' + err));
};

exports.updateDish = function (req, res) {
    Dish.findOneAndUpdate({ _id: req.params.id }, req.body, { new: true }, function (err, updatedDish) {
        if (err) {
            res.status(404).send({ success: false, message: 'Something bad happened' });
        } else {
            res.status(200).send({ success: true, message: 'Dish updated into database!' })
        }

    });
};

exports.getDish = function (req, res) {
    Dish.find({ _id: req.params.id })
        .populate('comments.author')
        .then((dish) => {
            res.status(200).send(dish);
        }, (err) => next(err))
        .catch((err) => next(err));
};

exports.deleteDish = function (req, res) {
    Dish.remove({ _id: req.params.id }, function (err, dish) {
        if (err) {
            console.log(err.errors);
            res.status(404).send({ success: false, message: 'Not found!' });
        } else {
            res.status(200).send({ success: true, message: 'Dish successfully deleted' });
        }
    })
};

exports.addComment = function (req, res, next) {
    Dish.findById(req.params.id)
        .populate('comments.author')
        .then((dish) => {
            if (dish != null) {
                req.body.author = req.user._id;
                dish.comments.push(req.body);
                dish.save()
                    .then((dish) => {
                        Dish.findById(dish._id)
                            .populate('comments.author')
                            .then((dish) => {
                                res.status(200).send(dish);
                            });
                    }), (err) => next(err)
            } else {
                res.status(404).send({ success: false, message: 'Not found!' });
                return next(err);
            }
        }, (err) => next(err))
        .catch((err) => next(err));
};

exports.updateComment = function (req, res, next) {
    Dish.findById(req.params.id)
        .populate('comments.author')
        .then((dish) => {
            if (dish != null && dish.comments.id(req.params.commentId) != null) {
                if (req.user.id === dish.comments.id(req.params.commentId).author._id) {
                    if (req.body.rating) {
                        dish.comments.id(req.params.commentId).rating = req.body.rating;
                    }
                    if (req.body.comment) {
                        console.log("author ---- " + dish.comments.id(req.params.commentId).author._id);
                        console.log("user ---- " + req.user.id);
                        dish.comments.id(req.params.commentId).comment = req.body.comment;
                    }
                    dish.save()
                        .then((dish) => {
                            Dish.findById(dish._id)
                                .populate('comments.author')
                                .then((dish) => {
                                    res.status(200).send(dish);
                                })
                        }), (err) => next(err)
                } else {
                    res.status(403).send({ success: false, message: 'Forbidden!' });
                }
            } else {
                res.status(404).send({ success: false, message: 'Not found!' });
                return next(err);
            }
        }, (err) => next(err))
        .catch((err) => next(err));
};

exports.deleteComment = function (req, res, next) {
    Dish.find({ _id: req.params.id })
        .populate('comments.author')
        .then((dish) => {
            if (dish != null && dish.comments._id(req.params.commentId) != null) {
                if (req.user.id === dish.comments.id(req.params.commentId).author._id) {
                    dish.comments.id(req.params.commentId).remove();
                    dish.save()
                        .then((dish) => {
                            Dish.findById(dish._id)
                                .populate('comments.author')
                                .then((dish) => {
                                    res.status(200).send(dish);
                                })
                        }), (err) => next(err)
                } else {
                    res.status(403).send({ success: false, message: 'Forbidden!' });
                }
            } else {
                res.status(404).send({ success: false, message: 'Not found!' });
                return next(err);
            }
        }, (err) => next(err))
        .catch((err) => next(err));
};

exports.getComments = function (req, res, next) {
    Dish.find({ _id: req.params.id })
        .populate('comments.author')
        .then((dish) => {
            if (dish != null) {
                res.status(200).send(dish.comments);
            } else {
                res.status(404).send({ success: false, message: 'Not found!' });
                return next(err);
            }
        }, (err) => next(err))
        .catch((err) => next(err));
};

exports.getComment = function (req, res, next) {
    Dish.find({ _id: req.params.id })
        .populate('comments.author')
        .then((dish) => {
            if (dish != null && dish.comments.id(req.params.commentId) != null) {
                res.status(200).send(dish.comments.id(req.params.commentId));
            } else {
                res.status(404).send({ success: false, message: 'Not found!' });
                return next(err);
            }
        }, (err) => next(err))
        .catch((err) => next(err));
};
