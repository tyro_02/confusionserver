'use strict';

var Favourite = require('../models/favourites');
var Dish = require('../models/dish');
var bodyParser = require('body-parser')

exports.addFavourite = function (req, res, next) {
    var favourite;
    Dish.findById(req.params.dishId)
        .then((dish) => {
            if (dish != null) {
                Favourite.find({ 'user': req.user._id })
                    .then((fav) => {
                        if ((fav === null) || (fav.length === 0)) {
                            favourite = new Favourite();
                        } else {
                            favourite = fav[0];
                        }
                        favourite.user = req.user._id;
                        if (!favourite.dishes.includes(dish._id))
                            favourite.dishes.push(dish._id);
                        favourite.save()
                            .then((favourite) => {
                                Favourite.findById(favourite._id)
                                    .then((favourite) => {
                                        return res.status(200).send(favourite);
                                    });
                            }), (err) => next(err)
                                .catch((err) => next(err));
                    })

            } else {
                res.status(404).send({ success: false, message: 'Dish Not found!' });
                return next(err);
            }
        }, (err) => next(err))
        .catch((err) => next(err));
};

exports.removeFavourite = function (req, res, next) {
    var favourite;
    Dish.findById(req.params.dishId)
        .then((dish) => {
            if (dish != null) {
                Favourite.find({ 'user': req.user._id })
                    .then((fav) => {
                        if ((fav === null) || (fav.length === 0)) {
                            return res.status(200).send({ success: true, message: 'User did not have this dish as favourite!' });
                        } else {
                            favourite = fav[0];
                            if (favourite.dishes.includes(dish._id)) {
                                favourite.dishes.remove(dish._id);
                                favourite.save()
                                    .then((favourite) => {
                                        Favourite.findById(favourite._id)
                                            .then((favourite) => {
                                                return res.status(200).send(favourite);
                                            });
                                    }), (err) => next(err)
                                        .catch((err) => next(err));
                            } else {
                                return res.status(200).send({ success: true, message: 'User did not have this dish as favourite!' });
                            }

                        }

                    })

            } else {
                res.status(404).send({ success: false, message: 'Dish Not found!' });
                return next(err);
            }
        }, (err) => next(err))
        .catch((err) => next(err));
};


exports.getFavourites = function (req, res, next) {
    Favourite.find({ 'user': req.user._id })
        .populate('user')
        .populate('dishes')
        .then((favourites) => {
            res.status(200).send(favourites);
        }).catch((err) => {
            // console.log(err.errors);
            return res.status(404).send(err);
        });

};


exports.removeFavourites = function (req, res, next) {
    Favourite.deleteMany({ 'user': req.user._id })
        .then((result) => {
            return res.status(200).send(result);
        }).catch((err) => {
            console.log('in error ' + err);
            return res.status(404).send(err);
        });

};

exports.addFavourites = function (req, res, next) {
    var favourite;
    Favourite.find({ 'user': req.user._id })
        .then((fav) => {
            if ((fav === null) || (fav.length === 0)) {
                favourite = new Favourite();
            } else {
                favourite = fav[0];
            }
            favourite.user = req.user._id;
            for (var i = 0; i < req.body.length; i++) {
                if (!favourite.dishes.includes(req.body[i]._id))
                    favourite.dishes.push(req.body[i]._id);
            }

            favourite.save()
                .then((favourite) => {
                    Favourite.findById(favourite._id)
                        .then((favourite) => {
                            return res.status(200).send(favourite);
                        });
                }), (err) => next(err)
                    .catch((err) => next(err));
        })

}       