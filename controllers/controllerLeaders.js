'use strict';

var Leader = require('../models/leader'); 

exports.addLeader = function(req, res) {
    var l = new Leader(); 
    l.name = req.body.name;
    l.image = req.body.image;
    l.designation = req.body.designation;
    l.abbr = req.body.abbr;
    l.description = req.body.description;
    l.featured = req.body.featured;

    l.save(function (err, newLeader) {
        if (err) {
            console.log(err);
            return res.status(400).send({ success: false, message: 'Something Bad Happened!'}); 
        }
        res.status(201).send({ success : true , message: 'New Leader Inserted Into Database!' }) 
    }) 
}; 

exports.getLeaders = function(req, res) {
    Leader.find({}, function (err, leaders) {  
        if (err) {  
			console.log(err.errors);
        	return res.status(404).send({ success: false, message: 'Not found!'}); 
        } else {
           return res.status(200).send(leaders);
        }          
    });
}

exports.updateLeader = function(req, res) {
    Leader.findOneAndUpdate({_id: req.params.id}, req.body, {new: true}, function(err, updatedLeader) {
        if (err){
            res.status(404).json({success : false, message : 'Something bad happened' }); 
        } else {
            res.status(200).json({ success : true, message: 'Leader updated into database!' }) 
        }
        
    });
  };

exports.getLeader = function(req, res) {
    Leader.find({ _id: req.params.id }, function (err, leader) {  
        if (err) {  
        	return res.status(404).json({ success: false, message: 'Not found!'});    
        } else { 
            res.status(200).json(leader);  
        } 
    })  
};

exports.deleteLeader = function(req, res) {
    Leader.remove({ _id: req.params.id }, function (err, leader) {  
        if (err) {  
            console.log(err.errors);
        	return res.status(404).json({ success: false, message: 'Not found!'});    
        } else { 
            res.status(200).json({ success: true, message: 'Leader successfully deleted' }); 
        } 
    })  
};