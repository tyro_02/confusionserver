const mongoose = require('mongoose');
const User = require('./user');
const Dish = require('./dish');


const favouriteSchema = mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    dishes: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Dish'
    }]
},{
    timestamps: true
}, { usePushEach: true });

var Favourite = mongoose.model('Favourite', favouriteSchema);

module.exports = Favourite;