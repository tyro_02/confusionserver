var createError = require('http-errors');
var mongoose = require('mongoose'); 
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var passport = require('passport');
var config = require('./config/main');
var bodyParser = require('body-parser');

var indexRouter = require('./routes/index');

var dishRouter = require('./routes/dishRoutes');
var leaderRouter = require('./routes/leaderRoutes');
var promotionRouter = require('./routes/promotionRoutes');
var userRouter = require('./routes/userRoutes');
var uploadRouter = require('./routes/uploadRoute');
var favouritesRouter = require('./routes/favouritesRoutes');

// mongoose instance connection url connection
mongoose.Promise = global.Promise; 

mongoose.connect(config.database, { keepAlive: true, reconnectTries: Number.MAX_VALUE, useNewUrlParser: true, useCreateIndex: true })
.then(({db: conFusion}) => console.log(`Connected to conFusion db`))
.catch(err => console.error(err));

var app = express();
app.use(bodyParser.json({limit: '3mb', extended: true}));
app.use(bodyParser.urlencoded({limit: '3mb', extended: true}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(passport.initialize());

// Secure traffic only
app.all('*', (req, res, next) => {
  if (req.secure) {
    return next();
  }
  else {
    res.redirect(307, 'https://' + req.hostname + ':' + app.get('secPort') + req.url);
  }
});

app.use('/', indexRouter);
// app.use('/users', usersRouter);
dishRouter(app);
leaderRouter(app);
promotionRouter(app);
userRouter(app);
uploadRouter(app);
favouritesRouter(app);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
